# The Pedant - ملانقطی (mola-noghati)

This is a machine for finding and correcting misspellings of a Farsi/Persian input text.

At this moment, this script has the ability to find "هکسره" and "گذاشتن/گزاردن" problems.

**Exmaple**:

input: `یه تیکه پنیره جذابه خوشمزه گزاشتم روی میز پذیراییه خونه`

output: `یه تیکه پنیر جذاب خوشمزه گذاشتم روی میز پذیرایی خونه`




# Tip

 It's good to know, "ملالغتی" or "ملاغلطی" are wrong. The correct word is "ملانقطی"

A "Pedant" or "ملانقطی" is a person who is excessively concerned with formalism, accuracy, and precision, or one who makes an ostentatious and arrogant show of learning. 


# Requirements

Hazm library: `pip install hazm`

parsivar library: `pip install parsivar`

# Warning

This script may could not find some mistakes or maybe it changes some correct words wrongly.
I did anything to have the best output, but there are some problems with libraries or coversational writing in Farsi/Persian.
If you find this kind of problems, please raise it in issues.
