from __future__ import unicode_literals
from hazm import Stemmer
hazmstemmer = Stemmer()

from parsivar import POSTagger
from parsivar import Normalizer
from parsivar import Tokenizer
from parsivar import SpellCheck
myspell_checker = SpellCheck()
my_normalizer = Normalizer()
my_tokenizer = Tokenizer()
my_tagger = POSTagger(tagging_model="wapiti")

def textpreprocess(chat):
    chat = chat.replace('منو', 'من را')
    chat = chat.replace('منه' , 'من است')
    chat = chat.replace('همینه','همین است')
    chat = chat.replace('اگه','اگر')
    return my_normalizer.normalize(chat)

gozargozar=['بزار','گزاشتند','گزاشتید','گزاشت','گزا','گزاشتن']
shanbe ='شنبه'
endh=['سپرده','شمرده','دیده']
def verbchecker(text):
    stemmed=hazmstemmer.stem(text)
    if stemmed in gozargozar:
        a="ز"
        b='ذ'
        text = text.replace(a,b)
        return True , text
    else: 
        return False , text
    
    

def main(chat):
    checker = chat
    chat = textpreprocess(chat)
    tokenized = my_tokenizer.tokenize_words(chat)
    text_tags = my_tagger.parse(tokenized)
    for i in range(0,len(text_tags)):
        status , sahih = verbchecker(tokenized[i])
        if status:
            chat = chat.replace(tokenized[i],sahih)
        if 'N_SING' in text_tags[i] or 'N_PL' in text_tags[i]: # target is singular or plural noun
            if i != len(tokenized)-1:
                if 'PRO' in text_tags[i+1] or 'ADJ' in text_tags[i+1]: # to find out noun has adjective or genitive 
                    if tokenized[i][-1] == "ه" and shanbe not in myspell_checker.spell_corrector(tokenized[i]) and myspell_checker.spell_corrector(tokenized[i]) not in endh:
                            #chat = chat.replace(tokenized[i],tokenized[i].replace(tokenized[i][-1],"*"))
                            word = list(tokenized[i])
                            word[-1] =''
                            word = ''.join(word)
                            if 'V' not in my_tagger.parse(word):
                                chat = chat.replace(tokenized[i],word)
                                chat = main(chat)
        
 

    return chat
